/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	
	alert("Hello! I just want to ask some basic information about you.");

	let fnamePrompt = prompt("What is your Full Name?");
	let agePrompt = prompt("How old are you?");
	let locPrompt = prompt("Where do you live?");

	console.log("Thank you for answering, " + fnamePrompt + ". Your age is " + agePrompt + " years old and you are located at " + locPrompt + ".");

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favBands(){
		console.log("1. The Cab");
		console.log("2. Lany");
		console.log("3. Rex Orange County");
		console.log("4. Silent Sanctuary");
		console.log("5. Lola Amor");
	}
	favBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favMovies(){
		let firstMovie = "1. The Last Five Year\nRotten Tomatoes Rating: 60%";
		let secondMovie = "2. Close To You\nRotten Tomatoes Rating: 89%";
		let thirdMovie = "3. In Time\nRotten Tomatoes Rating: 37%";
		let fourthMovie = "4. Blackpink: The Movie\nRotten Tomatoes Rating: 74%";
		let fifthMovie = "5. Ella Enchanted\nRotten Tomatoes Rating: 93%";

		console.log(firstMovie);
		console.log(secondMovie);
		console.log(thirdMovie);
		console.log(fourthMovie);
		console.log(fifthMovie);
	}
	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
